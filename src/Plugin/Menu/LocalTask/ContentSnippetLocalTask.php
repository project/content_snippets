<?php

namespace Drupal\content_snippets\Plugin\Menu\LocalTask;

use Drupal\Core\Menu\LocalTaskDefault;
use Symfony\Component\HttpFoundation\Request;

/**
 * Local task plugin to render dynamic tab title using Content Snippets.
 */
class ContentSnippetLocalTask extends LocalTaskDefault {

  /**
   * {@inheritdoc}
   */
  public function getTitle(Request $request = NULL) {
    return content_snippets_retrieve((string) $this->pluginDefinition['snippet']);
  }

}
