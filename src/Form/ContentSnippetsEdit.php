<?php

namespace Drupal\content_snippets\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Custom Texts.
 */
class ContentSnippetsEdit extends ConfigFormBase {

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'content_snippets_edit';
  }

  /**
   * Gets the configuration names that will be editable.
   *
   * @return array
   *   An array of configuration object names that are editable if called in
   *   conjunction with the trait's config() method.
   */
  protected function getEditableConfigNames() {
    return ['content_snippets.items'];
  }

  /**
   * Settings for "Custom Text" form.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   Form definition array.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = content_snippets_config();
    $content = content_snippets_retrieve_all();

    $form['header'] = [
      '#type' => 'markup',
      '#markup' => '<h3>Set custom text used on the site.</h3><p>Configure text that appears on the site outside of specific pieces of content or blocks. Use with care: the effect is immediate.</p>',
    ];
    foreach ($config as $snip_id => $snip_config) {
      $element = [
        '#title' => $snip_config['label'],
        '#default_value' => $content[$snip_id] ?? '',
        '#type' => $snip_config['type'],
        '#description' => $snip_config['description'],
        '#weight' => $snip_config['weight'],
      ];
      if ($snip_config['type'] == 'text_format' && !empty($snip_config['filter'])) {
        $element['#format'] = $snip_config['filter'];
      }
      if (!empty($snip_config['group'])) {
        if (empty($form[$snip_config['group']])) {
          $form[$snip_config['group']] = [
            '#type' => 'details',
            '#title' => $snip_config['group'],
            '#open' => TRUE,
            '#weight' => $snip_config['weight'],
          ];
        }
        $form[$snip_config['group']][$snip_id] = $element;
      }
      else {
        $form[$snip_id] = $element;
      }
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $snippets = $this->configFactory()->getEditable('content_snippets.content');
    $snippets_config = $this->configFactory()->getEditable('content_snippets.items');
    $values = $form_state->cleanValues()->getValues();
    $snippet_content = $snippets->get('snippets') ?? [];
    foreach ($values as $field_key => $field_value) {
      $snip_config = $snippets_config->get($field_key);
      if ($snip_config['type'] == 'text_format') {
        $snippet_content[$field_key] = $field_value['value'];
        $snip_config['filter'] = $field_value['format'];
        $snippets_config->set($field_key, $snip_config);
      }
      else {
        $snippet_content[$field_key] = $field_value;
      }
    }
    $snippets->set('snippets', $snippet_content);
    $snippets->save();
    $snippets_config->save();
    parent::submitForm($form, $form_state);
  }

}
