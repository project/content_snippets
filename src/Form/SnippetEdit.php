<?php

namespace Drupal\content_snippets\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure Custom Texts.
 */
class SnippetEdit extends ConfigFormBase {

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->moduleHandler = $container->get('module_handler');
    return $instance;
  }

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'content_snippets_edit_snip';
  }

  /**
   * Gets the configuration names that will be editable.
   *
   * @return array
   *   An array of configuration object names that are editable if called in
   *   conjunction with the trait's config() method.
   */
  protected function getEditableConfigNames() {
    return ['content_snippets.items'];
  }

  /**
   * Settings for "Custom Text" form.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param String $snip_id
   *   The machine name of the snip to edit.
   *
   * @return array
   *   Form definition array.
   */
  public function buildForm(array $form, FormStateInterface $form_state, String $snip_id = '') {

    $form = parent::buildForm($form, $form_state);

    $typeoptions = [
      'number' => 'Number',
      'textfield' => 'Line (plain text)',
      'textarea' => 'Paragraph (plain text)',
    ];
    if ($this->moduleHandler->moduleExists('filter')) {
      $typeoptions['text_format'] = 'Paragraph (formatted)';
    }
    if (!empty($snip_id)) {
      $snip_info = $this->config('content_snippets.items')->get($snip_id);
    }
    else {
      $snip_info = NULL;
    }
    $form['label'] = [
      '#title' => $this->t('Label'),
      '#type' => 'textfield',
      '#length' => 50,
      '#default_value' => $snip_info ? $snip_info['label'] : '',
      '#description' => $this->t('The label that content editors will see when editing Snippet contents.'),
    ];

    $form['id'] = [
      '#title' => 'Snippet ID',
      '#type' => 'machine_name',
      '#default_value' => $snip_id,
      '#disabled' => !is_null($snip_info),
      '#maxlength' => 64,
      '#description' => $this->t('A unique machine ID for this item, which will be used to reference it in code. It must only contain lowercase letters, numbers, and underscores.'),
      '#machine_name' => [
        'exists' => 'Drupal\content_snippets\Form\SnippetEdit::checkExisting',
        'label' => 'Snipped ID',
      ],
    ];


    $form['type'] = [
      '#title' => $this->t('Snippet Type'),
      '#type' => 'select',
      '#default_value' => $snip_info ? $snip_info['type'] : NULL,
      '#options' => $typeoptions,
    ];
    if (!empty($snip_info)) {
      $form['type']['#description'] = $this->t('WARNING: Changing the type of an existing snippet may destroy the existing snippet content.');
    }

    if (isset($typeoptions['text_format'])) {
      $formats = [];
      foreach (filter_formats() as $id => $format) {
        $formats[$id] = $format->label();
      }
      $form['filter'] = [
        '#title' => $this->t('Wysiwyg Format'),
        '#type' => 'select',
        '#default_value' => isset($snip_info['filter']) ? $snip_info['filter'] : NULL,
        '#options' => $formats,
        '#states' => [
          'visible' => [
            ':input[name="type"]' => ['value' => 'text_format'],
          ],
        ],
      ];
    }
    $form['description'] = [
      '#title' => $this->t('Help text'),
      '#type' => 'textarea',
      '#default_value' => $snip_info ? $snip_info['description'] : '',
      '#description' => $this->t('A description of where this snippet is used, to help content editors know where to use it. You can include HTML: for example, linking to where this content appears.'),
    ];

    $form['group'] = [
      '#title' => $this->t('Group'),
      '#type' => 'textfield',
      '#default_value' => $snip_info ? $snip_info['group'] : '',
      '#size' => 20,
    ];

    $form['weight'] = [
      '#title' => $this->t('Weight'),
      '#type' => 'number',
      '#default_value' => $snip_info ? $snip_info['weight'] : 10,
      '#size' => 4,
    ];

    $form['actions']['submit']['#value'] = $this->t('Save');
    return $form;
  }

  /**
   * Validation callback for the "name" machinename field.
   */
  public static function checkExisting($value, array $element, FormStateInterface $form_state) {
    return (bool) content_snippets_retrieve($value);
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $items = $this->configFactory()->getEditable('content_snippets.items');
    $values = $form_state->cleanValues()->getValues();
    if (!empty($values['id'])) {
      $items->set($values['id'], $values);
    }
    $items->save();
    parent::submitForm($form, $form_state);
  }

}
