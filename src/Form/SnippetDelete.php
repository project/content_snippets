<?php

namespace Drupal\content_snippets\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;


/**
 * Delete a Custom Text Snippet.
 */
class SnippetDelete extends ConfigFormBase {

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->configFactory = $container->get('config.factory');
    $instance->messenger = $container->get('messenger');
    return $instance;
  }

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'content_snippets_delete_snip';
  }

  /**
   * Gets the configuration names that will be editable.
   *
   * @return array
   *   An array of configuration object names that are editable if called in
   *   conjunction with the trait's config() method.
   */
  protected function getEditableConfigNames() {
    return ['content_snippets.items'];
  }

  /**
   * Deletion confirmation form for a Snippet.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param String $snip_id
   *   The machine name of the snip to delete.
   *
   * @return array
   *   Form definition array.
   */
  public function buildForm(array $form, FormStateInterface $form_state, String $snip_id = '') {

    $form = parent::buildForm($form, $form_state);
    $form['snip_id'] = [
      '#type' => 'hidden',
      '#value' => $snip_id,
    ];
    $form['actions']['cancel'] = [
      '#title' => $this->t('Cancel'),
      '#type' => 'link',
      '#url' => Url::fromRoute('content_snippets.config.snippets_config'),
      '#attributes' => ['class' => 'button'],
    ];

    $snip_info = $this->config('content_snippets.items')->get($snip_id);

    if (empty($snip_info)) {
      $snip_info = NULL;
      $form['message'] = [
        '#markup' => $this->t("No content snippet found with ID: $snip_id")
      ];
      unset($form['actions']['submit']);
      return $form;
    }
    $form['message'] = [
      '#markup' => $this->t('Are you sure you want to delete the snippet %name with machine name %snip_id?',[
        '%name' => $snip_info['label'],
        '%snip_id' => $snip_id,
      ]),
      '#description' => $this->t('Before deleting this snippet, it is recommended that you search your site codebase to ensure the machine name is not in use anywhere.'),
    ];
    $content = $this->configFactory->get('content_snippets.content')->get($snip_id);

    $form['content'] = [
      '#prefix' => '<br /><h3>Current Content:</h3>',
      '#markup' => !empty($content) ? $content : $this->t('(Snippet has no content)'),
    ];

    $form['actions']['submit']['#value'] = $this->t('Delete');
    return $form;
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $snip_id = $form_state->getValue('snip_id');
    $content = $this->configFactory()->getEditable('content_snippets.content');
    $content->clear($snip_id);
    $content->save();
    $items = $this->configFactory()->getEditable('content_snippets.items');
    $items->clear($snip_id);
    $items->save();
    $form_state->setRedirect('content_snippets.config.snippets_config');
    $this->messenger->addMessage("$snip_id deleted.");
    parent::submitForm($form, $form_state);
  }

}
