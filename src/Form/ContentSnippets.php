<?php

namespace Drupal\content_snippets\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Configure Custom Texts.
 */
class ContentSnippets extends ConfigFormBase {

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'content_snippets';
  }

  /**
   * Gets the configuration names that will be editable.
   *
   * @return array
   *   An array of configuration object names that are editable if called in
   *   conjunction with the trait's config() method.
   */
  protected function getEditableConfigNames() {
    return ['content_snippets.items'];
  }

  /**
   * Settings for "Custom Text" form.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   Form definition array.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form = parent::buildForm($form, $form_state);

    $form['header'] = [
      '#type' => 'markup',
      '#markup' => $this->t('<h3>Create custom text snippets for use on the site.</h3><p>The contents of the snippets can be edited by Content Editors. The snippets can be used in code, either using <code>content_snippets_retrieve(snippetid)</code>, with tokens <code>[content_snippets:snippetid]</code> or in template files using <code>{{ contentSnippets.snippetid }}</code>.</p>'),
    ];

    $config = content_snippets_config();
    // Sort snippets by group, then weight, then ID.
    usort($config, function ($a, $b) {
      if ($a['group'] === $b['group']) {
        if ($a['weight'] === $b['weight']) {
          return 0;
        }
        else {
          return ($a['weight'] > $b['weight']) ? 1 : -1;
        }
      }
      return ($a['group'] > $b['group']) ? 1 : -1;
    });
    foreach ($config as $index => $snippet) {
      unset($config[$index]['description']);
      if (isset($config[$index]['filter'])) {
        $config[$index]['type'] .= ' (' . $config[$index]['filter'] . ')';
        unset($config[$index]['filter']);
      }
      $editurl = Url::fromUserInput('/admin/config/content_snippets/admin/' . $snippet['id'] . '/edit');
      $deleteurl = Url::fromUserInput('/admin/config/content_snippets/admin/' . $snippet['id'] . '/delete');
      $config[$index]['edit'] = Link::fromTextAndUrl('edit', $editurl);
      $config[$index]['delete'] = Link::fromTextAndUrl('delete', $deleteurl);
    }
    $form['snippets'] = [
      '#type' => 'table',
      '#title' => 'Current Snippets',
      '#header' => ['Name', 'ID', 'Type', 'Group', 'Weight', 'Links', ''],
      '#rows' => $config,
      '#footer' => NULL,
      '#caption' => NULL,
      '#sticky' => FALSE,
      '#responsive' => TRUE,
      '#attributes' => ['id' => 'snippets'],
    ];

    $form['actions']['submit']['#value'] = $this->t('Save');
    return $form;
  }

}
