<?php

namespace Drupal\content_snippets;

use Drupal\Core\Render\RenderableInterface;

/**
 * Renderable version of a content snippet.
 */
class RenderableSnippet implements RenderableInterface {

  /**
   * The text of the snippet.
   *
   * @var string
   */
  protected $text;

  /**
   * The filter format for the snippet.
   *
   * @var string
   */
  protected $filterFormat;

  /**
   * Constructs a new Link object.
   *
   * @param string $text
   *   The text of the link.
   * @param string $filterFormat
   *   The url object.
   */
  public function __construct($text, $filterFormat) {
    $this->text = $text;
    $this->filterFormat = $filterFormat;
  }

  /**
   * {@inheritdoc}
   */
  public function toRenderable() {
    return [
      '#type' => 'processed_text',
      '#text' => $this->text,
      '#format' => $this->filterFormat,
    ];
  }

}
