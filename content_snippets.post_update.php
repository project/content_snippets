<?php

/**
 * @file
 * Contains post_install function.
 */

/**
 * Restructure existing content Snippets.
 */
function content_snippets_post_update_restructure_config() {
  $config = \Drupal::configFactory()->getEditable('content_snippets.content');
  $new_config = [
    'snippets' => [],
  ];
  foreach ($config->get() as $id => $value) {
    if ($id === '_core') {
      $new_config[$id] = $value;
    }
    else {
      $new_config['snippets'][$id] = $value;
    }
  }
  $config->setData($new_config);
  $config->save();
}
