<?php

/**
 * @file
 * Contains content_snippets tokens hooks.
 */

use Drupal\Core\Render\BubbleableMetadata;

/**
 * Implements hook_token_info().
 */
function content_snippets_token_info() {
  $info['types']['content_snippets'] = [
    'name' => t('Content Snippets'),
    'description' => t("Custom token type for site-wide Content Snippets."),
  ];
  $config = content_snippets_config();
  $info['tokens']['content_snippets'] = [];
  foreach ($config as $key => $value) {
    // Site-wide global token.
    $info['tokens']['content_snippets'][$key] = [
      'name' => $key,
      'description' => $value['description'],
      'module' => 'content_snippets',
    ];
  }

  return $info;
}

/**
 * Implements hook_tokens().
 */
function content_snippets_tokens($type, $tokens, array $data, array $options, BubbleableMetadata $bubbleable_metadata) {
  $replacements = [];

  if ($type == 'content_snippets') {
    foreach ($tokens as $name => $original) {
      $replacements[$original] = content_snippets_retrieve($name);
    }
  }
  return $replacements;
}
